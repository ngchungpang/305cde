        // Initialize Firebase
        var config = {
            apiKey: "AIzaSyAqxPGWyG3w7Ns_-Yyj6K63lOPqgX2nFww",
            authDomain: "shoppingcat-6beee.firebaseapp.com",
            databaseURL: "https://shoppingcat-6beee.firebaseio.com",
            projectId: "shoppingcat-6beee",
            storageBucket: "",
            messagingSenderId: "759387200186"
        };
        firebase.initializeApp(config);

        //save item list array
        var productList = [];

        //use the promise to check the firebase
        function getFirebaseData() {
            return new Promise(function(resolve, reject) {
                firebase.database().ref('item/').once('value', function(snapshot) {
                    console.log(snapshot.val());
                    if (snapshot) {
                        resolve(snapshot);
                    }
                    else {
                        reject("Cannot cannect firebase OR the database no data!");
                    }
                });
            });
        }
        //control the promise result
        getFirebaseData().then(
            function(data) {
                listItem(data);
            },
            function(data) {
                alert(data);
            }
        );


        //show the item from firebase
        function listItem(snapshot) {
            snapshot.forEach(function(data) {

                var id = data.val().id;
                var title = data.val().title;
                var desc = data.val().desc;
                var pic = data.val().pic;
                var cost = data.val().cost;
                var arr = [id, title, pic, desc, cost];
                //alert("db data :" + arr);
                productList.push(arr);

                var html = '<div class="itemInfo" >' +
                    '<div class="title">' + title + '</div>' +
                    '<div><img class="pic" src="' + pic + '"></div>' +
                    '<div><textarea class="desc" readonly >' + desc +
                    '</textarea></div>' +
                    '<div>Item Price : $<span class="pirce">' + cost + '</span></div>' +
                    '<div><button class="addBtn" onclick="addToCart(\'' + id + '\')" >Add To Basket</button></div></div>';
                $('#itemView').append(html);


            });
        }

        // basket object use the module and closures
        var basket = {
            baskets: [],
            totalCost: function() {
                var totalCost = 0;
                for (var i = 0; i < this.baskets.length; i++) {
                    totalCost += this.baskets[i].totalCost();
                }
                return totalCost;
            },
            totalQty: function() {
                var totalQty = 0;
                for (var i = 0; i < this.baskets.length; i++) {
                    totalQty += this.baskets[i].getQty();
                }
                return totalQty;

            },
            showItemToBasket: function() {
                var item = this.baskets.pop();
                this.baskets.push(item);
                //show item to basket
                var html = '<div class="basketItem" data-id="' + item.id + '" >' +
                    '<button class="basketItemRemove" onclick="basket.removeItem(\'' + item.id + '\',this)">Remove</button>' +
                    '<div class="bItemTitle">' + item.title + '</div>' +
                    '<div><textarea class="desc" readonly >' + item.desc +
                    '</textarea></div>' +
                    '<div class="basketItemInfo">Cost : $' + item.cost + ' | Qty : <input class="basketItemQty" onchange="basket.updateItemQty(\'' + item.id + '\',this)" type="number" min=1 max=100 value="' + item.qty + '" /> | TotalCost : $<span class="itemTotalCost">' + item.totalCost() + '</span></div><hr></div>';
                $('#itemList').append(html);
                this.updateOrder();
            },
            addItemToCart: function(item) {
                this.baskets.push(item);
                alert("The item add to baske successfull!");
            },
            removeItem: function(itemid, obj) {

                for (var i = 0; i < this.baskets.length; i++) {
                    if (itemid == this.baskets[i].id) {
                        this.baskets.splice(i, 1);
                        $(obj).parent('.basketItem').remove();
                        this.updateOrder();
                        this.showBasketsTotal();
                    }
                }
                this.showBasketsTotal();
            },
            updateItemQty: function(itemid, obj) {
                var qty = obj.value;
                for (var i = 0; i < this.baskets.length; i++) {
                    if (itemid == this.baskets[i].id) {
                        this.baskets[i].setQty(qty);
                        $(obj).parent('.basketItemInfo').children(".itemTotalCost").html(this.baskets[i].totalCost());
                        this.updateOrder();
                        return;
                    }
                }
            },
            inBasket: function(itemid) {
                for (var i = 0; i < this.baskets.length; i++) {
                    if (itemid == this.baskets[i].id) {
                        return 0;
                    }
                }
                return 1;
            },
            showBasketsTotal: function() {
                $('#basketQty').html(this.baskets.length);
            },
            updateOrder: function() {
                $('#basketTotalQty').html(this.totalQty());
                $('#basketTotalCost').html(this.totalCost());
            }

        };
        
           //item object use the module and closures
        var item = function() {
            var id;
            var title;
            var desc;
            var cost;
            var pic;
            var qty;

            return {
                newItem: function(id, title, pic, desc, cost, qty) {
                    this.id = id;
                    this.title = title;
                    this.pic = pic;
                    this.desc = desc;
                    this.cost = cost;
                    this.qty = qty;
                },
                totalCost: function() {
                    var total = (this.qty * this.cost)
                    return total;
                },
                setQty: function(qty) {
                    this.qty = qty;
                },
                getQty: function() {
                    return parseInt(this.qty);
                }
            }
        };

        //item on clieck add to basket event
        function addToCart(itemid) {
            //check the item in the cart? yes show error message
            if (basket.inBasket(itemid) == 0) {
                alert("This item in the basket!")
                return;
            };

            //if the cart no this item , put it into cart
            var id;
            var title;
            var pic;
            var desc;
            var cost;

            for (var i = 0; i < productList.length; i++) {
                if (productList[i][0] == itemid) {
                    id = productList[i][0];
                    title = productList[i][1];
                    pic = productList[i][2]
                    desc = productList[i][3];
                    cost == productList[i][4];
                    cost = productList[i][4];
                    var arr = [id, title, pic, desc, cost];
                    break;

                }
            }
            var newBasketItem = new item();
            newBasketItem.newItem(id, title, pic, desc, cost, 1);
            newBasketItem.cost;
            basket.addItemToCart(newBasketItem);
            basket.showBasketsTotal();
            basket.showItemToBasket();



        }
     
        