var request = require("request");
var cheerio = require("cheerio");

// 台南市的氣溫
var url = "http://www.aastocks.com/tc/stocks/market/index/hk-index-con.aspx?index=HSI";

// 取得網頁資料
request(url, function (error, response, body) {
  if (!error) {

    // 用 cheerio 解析 html 資料
    var $ = cheerio.load(body);

    // 篩選有興趣的資料
    var temperature = $("#hkIdxContainer .hkidx-last txt_r").html();
 

    // 輸出
    document.write("Hello World!");
    console.log("氣溫：攝氏 " + temperature + " 度");

  } else {
    console.log("擷取錯誤：" + error);
  }
});