const express = require('express');
const router = express.Router();
const admin = require('firebase-admin');
const serviceAccount = require('../serviceAccountKey.json');


//init firebase 
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});
//connect firebase
var db = admin.firestore();
var ref = db.collection('restaurants');


//model for used by closures and modules
var restaurants = function() {
    var id, name, address, lat, lng, rating, area;

    return {
        newRestaurant: function(id, name, address, lat, lng, rating, area) {
            this.id = id;
            this.name = name;
            this.address = address;
            this.lat = lat;
            this.lng = lng;
            this.rating = rating;
            this.area = area;
        },
        getJson: function() {
            var jsonStr = {
                id: this.id,
                name: this.name,
                address: this.address,
                lat: this.lat,
                lng: this.lng,
                rating: this.rating,
                area: this.area
            }
            return jsonStr;

        }
    };
};
var restaurantsList = {
    list: [],
    count: function() {
        return this.list.length;
    },
    add: function(restaurants) {
        this.list.push(restaurants);
    },
    getJson: function() {
        var arrList = [];
        this.list.forEach(rest => {
            arrList.push(rest.getJson());
        });
        return arrList;

    }
}



//get method for search data ,print out the json
router.get('/', (req, res, next) => {
    //get parameter
    var area = req.query.area;
    //search from firebase
    ref.where("area", "==", area).get().then(snapshot => {
            console.log(snapshot.size);
            snapshot.forEach(doc => {
                var data = doc.data();
                var rest = new restaurants();
                rest.newRestaurant(doc.id, data.name, data.address, data.lat, data.lng, data.rating, data.area);
                restaurantsList.add(rest);

            });
            res.status(200).json({
                status: 'success ',
                results: restaurantsList.getJson(),
                count: restaurantsList.count()
            });
            restaurantsList.list = [];

        })
        .catch(err => {
            res.status(201).json({
                status: "error",
                message: err.message,
            });
        });
});
//post method for insert data 
router.post('/', (req, res, next) => {
    console.log(req.body);

    ref.add(req.body);

    res.status(200).json({
        status: 'success ',
        message: 'insert data is success',
        insertData: req.body

    }).catch(err => {
        res.status(201).json({
            status: "error",
            message: err.message,
        });
    });
});
//get method for get single record
router.get('/:restaurantsId', (req, res, next) => {
    const id = req.params.restaurantsId;
    if (id) {
        ref.doc(id).get().then(doc => {
                if (!doc.exists) {
                    res.status(200).json({
                        status: "success",
                        message: 'No Results ',
                    });
                }
                else {
                    res.status(200).json({
                        status: "success",
                        results: doc.data()
                    });
                }
            })
            .catch(err => {
                res.status(201).json({
                    status: "error",
                    message: err.message,
                });
            });

    }
    else {
        res.status(201).json({
            message: 'Data Not Found'
        });
    }
});
//patch method for update data
router.patch('/:restaurantsId', (req, res, next) => {
    const id = req.params.restaurantsId;
    var data = req.body;
    ref.doc(id).update(data).then(function(data) {
        res.status(200).json({
            status: "success",
            message: 'Updated restaurants!'
        });

    }).catch(err => {
        res.status(201).json({
            status: "error",
            message: err.message,
        });
    });

});
//delete method for delete data
router.delete('/:restaurantsId', (req, res, next) => {
    const id = req.params.restaurantsId;

    ref.doc(id).delete().then(function() {
        res.status(200).json({
            status: "success",
            message: 'Deleted restaurants!'
        });

    }).catch(err => {
        res.status(201).json({
            status: "error",
            message: err.message,
        });
    });
});

module.exports = router;
