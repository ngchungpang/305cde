const express = require('express');
const router = express.Router();
const request = require('request');
const admin = require('firebase-admin');
var nodemailer = require('nodemailer');
var randomstring = require("randomstring");

//connect firebase
var db = admin.firestore();
var ref = db.collection('users');
var ref2 = db.collection('favourite');


var transporter = nodemailer.createTransport({
    service: 'gmail',
    port: 587,
    auth: {
        user: 'ncp0804@gmail.com',
        pass: 'Rr2972866'
    }
});

//controll the index page, will check the user is login status , and provide more function 
router.get('/index', (req, res, next) => {
    var area = req.query.area;
    var ustatus = false;
    var uInfo;
    // check the user is login status
    if (req.cookies.uid === undefined) {
        ustatus = false;

    }
    else {
        ustatus = true;
        ref.doc(req.cookies.uid).get().then(snapshot => {
            console.log(snapshot.data());
            uInfo = snapshot.data().name;
            console.log(uInfo);
        });
    }
    
    setTimeout(function() {
        if (area) {

            var options = {
                url: 'https://cde305-ngchungpang.c9users.io/restaurants?area=' + area,
                method: 'GET',
                //json: requestData
                //  headers: {'Content-Type': 'application/json'}
            }
            // get the api data
            request.get(options, function(err, results, body) {
                if (!err && res.statusCode === 200) {
                    res.render('index', {
                        status: true,
                        ustatus: ustatus,
                        uInfo: uInfo,
                        results: JSON.parse(body)
                    });

                }
            });
        }
        else {
            console.log(uInfo);
            res.render('index', {
                status: false,
                ustatus: ustatus,
                uInfo: uInfo,
                results: "NO RESULTS"
            });
        }
    }, 1200);

});
//routing to sign page
router.get('/sign', (req, res, next) => {
    res.render('sign');

});
// handle the register 
router.post('/sign/register', (req, res, next) => { 
    ref.where("email", "==", req.body.email).get().then(snapshot => {
            if (snapshot.size == 0) {
                var randomStr = randomstring.generate(4);
                req.body.code = randomStr;
                req.body.verification = false;
                ref.add(req.body);
                res.status(200).json({
                    status: true,
                    message: "Register success!"
                });
                var mailOptions = {
                    from: 'ncp0804@gmail.com',
                    to: req.body.email,
                    subject: 'Register Verification',
                    text: 'The verification code : [ ' + randomStr + ' ]'
                };
                //send email
                transporter.sendMail(mailOptions, function(error, info) {
                    if (error) {
                        console.log(error);
                    }
                    else {
                        console.log('Email sent: ' + info.response);
                    }
                });
            }
            else {
                res.status(200).json({
                    status: false,
                    message: "The email is exist!"
                });
            }
        })
        .catch(err => {
            res.status(201).json({
                status: "error",
                message: err.message,
            });
        });


});

//handle the login
router.post('/sign/login', (req, res, next) => {
    //check the user is exist?
    ref.where("email", "==", req.body.email).where("pwd", "==", req.body.pwd).get().then(snapshot => {
            if (snapshot.size > 0) {
                snapshot.forEach(doc => {
                    var data = doc.data();

                    if (data.verification) {
                        //set cookies
                        res.cookie("uid", doc.id, { maxAge: 1000000 });
                        console.log("Cookies1 :  ", req.cookies.uid);
                        res.status(200).json({
                            status: true,
                            message: "Sign in is success!"
                        });
                    }
                    else {
                        res.status(200).json({
                            status: false,
                            message: "The account not confirmation, please confirm your account ,the code in your email."
                        });
                    }
                });
            }
            else {
                res.status(200).json({
                    status: false,
                    message: "Password or email not success!"
                });
            }

        })
        .catch(err => {
            res.status(201).json({
                status: "error",
                message: err.message,
            });
        });

});

//handle the account verification
router.post('/sign/verification', (req, res, next) => {
    ref.where("email", "==", req.body.email).get().then(snapshot => {
        if (snapshot.size > 0) {
            snapshot.forEach(doc => {
                var data = doc.data();
                // confirm the account
                if (data.code == req.body.code) {
                    ref.doc(doc.id).update({ verification: true }).then(function(data) {
                        res.status(200).json({
                            status: "true",
                            message: 'verification success!'
                        });
                    })
                }
                else {
                    res.status(200).json({
                        status: "false",
                        message: 'The verification code not match!'
                    });
                }
            });
        }
        else {
            res.status(200).json({
                status: "false",
                message: 'The email not success!'
            });
        }
    });
});

//routing to favourite page, desplay and handle the info data
router.get('/favourite', (req, res, next) => {
    console.log("favourite page");
    if (req.cookies.uid === undefined) {
        res.redirect('index');
    }
    var list = [];

    ref2.where("uid", "==", req.cookies.uid).get().then(snapshot => {
        if (snapshot.size > 0) {
            snapshot.forEach(doc => {
                var json = doc.data();
                json.id = doc.id;
                console.log(json);
                list.push(json);

            });
            res.render('favourite', {
                status: true,
                count: snapshot.count,
                results: list
            });
        }
        else {
            res.status(200).json({
                status: "false",
                message: 'Empty list!'
            });
        }
    });
});
//handle the add favourite item
router.post('/favourite/insert', (req, res, next) => {
    var options = {
        url: 'https://cde305-ngchungpang.c9users.io/restaurants/' + req.body.rid,
        method: 'GET',
        //json: requestData
        //  headers: {'Content-Type': 'application/json'}
    }
    //get api data
    request.get(options, function(err, results, body) {
        if (!err && res.statusCode === 200) {
            var data = JSON.parse(body);
            var rjson = data.results;
            rjson.apirid = req.body.rid;
            rjson.uid = req.cookies.uid;

            ref2.where("apirid", "==", req.body.rid).where("uid", "==", req.cookies.uid).get().then(snapshot => {
                if (snapshot.size > 0) {
                    res.status(200).json({
                        status: true,
                        message: "The favourite have this item !"
                    });

                }
                else {
                    ref2.add(rjson);
                    res.status(200).json({
                        status: true,
                        message: "Add favourite success!"
                    });
                }
            });
        }
        else {
            res.status(200).json({
                status: false,
                message: "Add favourite not success ."
            });
        }
    });
});

//handle  the  favourite item update
router.post('/favourite/update', (req, res, next) => {
    ref2.doc(req.body.fid).update({
        name: req.body.name,
        rating: req.body.rating,
        address: req.body.address,
        area: req.body.area

    }).then(function(data) {
        res.status(200).json({
            status: true,
            message: 'Updated You favourite restaurants!'
        });

    }).catch(err => {
        res.status(201).json({
            status: false,
            message: "update fail!",
        });
    });

});

//handle  the  favourite item delete
router.post('/favourite/delete', (req, res, next) => {
    const fid = req.body.fid;
    ref2.doc(fid).delete().then(function() {
        res.status(200).json({
            status: true,
            message: "Deleted!",
        });
    }).catch(err => {
        res.status(201).json({
            status: false,
            message: "delete fail!",
        });
    });
});

//logout user
router.get('/logout', (req, res, next) => {
    res.clearCookie("uid");
    res.redirect('index');
});

module.exports = router;
