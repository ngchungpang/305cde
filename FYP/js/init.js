function setCookie(cname, cvalue, hour) {
    var d = new Date();
    d.setTime(d.getTime() + (hour * 60 * 60 * 1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie(cname) {
    var user = getCookie(cname);
    if (user != "") {
        return true;
    }
    else {
        if (user != "" && user != null) {
            return false;
        }
    }
}

$(document).ready(function() {
    if (checkCookie("jsid")) {
        $("#signin").hide();
        $("#signup").hide();
        $("#logout").show();
        $("#profile").show();
        $("myapply").show();
    }
    else {
        $("#signin").show();
        $("#signup").show();
        $("myapply").hide();
        $("#logout").hide();
        $("#profile").hide();
        

    }

});
